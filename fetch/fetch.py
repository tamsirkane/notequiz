
do_until_js = '''
async function doUntil(action, frequency, condition, state, getState, target, witness, statesEqual, done) {
    state = (witness(state) === null) ? getState() : state;
    if (!condition(witness(state), witness(target))) {
      state = await changeState(state, getState, statesEqual, action, frequency);
      doUntil(action, frequency, condition, state, getState, target, witness, statesEqual, done)
    } else {
      done()
    }
}

function changeState(state, getState, statesEqual, action, frequency) {
  action();
  return new Promise(function executor(resolve, reject) {
    let newState = getState();

    if (!statesEqual(newState,state)) {
      resolve(newState);
    } else {
      setTimeout(executor, frequency, resolve, reject);
    }
  })
}
\n'''


scroll_js = '''
doUntil(
  () => window.scroll(0, 100000),
  50,
  (state, target) => state >= target,
  null,
  () => document.getElementsByClassName('userContentWrapper').length,
  arguments[0],
  x => x,
  (a, b) => (a === b),
  () => arguments[1]()
)
'''

expand_comments_js = '''
doUntil(
  () => document.evaluate('//*[contains(@class, "UFIPagerLink") and not(ancestor::div[contains(@class, "UFIImageBlockContent")]) ]' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click(),
  25,
  (state, target) => state === target,
  [null, null],
  () => [document.evaluate('//*[contains(@class, "UFIPagerLink") and not(ancestor::div[contains(@class, "UFIImageBlockContent")]) ]' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue,
         document.querySelectorAll('.UFIComment').length],
  [null, null],
  state => state[0],
  (a, b) => (a[1] === b[1]),
  () => arguments[1]()
)
'''

expand_post_js = '''
doUntil(
  () => arguments[0].querySelector('.userContent > :not(.text_exposed) > .text_exposed_hide >  .text_exposed_link > a').click(),
  50,
  (state, target) => state === target,
  null,
  () => arguments[0].querySelector('.userContent > :not(.text_exposed) > .text_exposed_hide >  .text_exposed_link > a'),
  null,
  x => x,
    (a, b) => (a === b),   // TODO: will create a bug, infinite loop, on posts with more than one 'afficher la suite'
  () => arguments[1]()
)
'''


parse_js = '''
function postToJson(post){
    let permalink = post.querySelector('.timestamp').parentElement.href;
    let postContent = post.querySelector('.userContent').textContent;
    let uficontainer = post.querySelector('.UFIContainer');
    let ufilist = uficontainer.querySelector('.UFIList');
    let comments = Array.from(ufilist.querySelectorAll(':not(.UFIReplyList) > .UFIComment'));
    let commentsJSON = comments.map(commentToJson);
    return {'permalink': permalink, 'content': postContent, 'comments': commentsJSON};
}


function commentToJson(com) {
    let author = com.querySelector('.UFICommentActorName').innerText;
    let content = com.querySelector('.UFICommentBody').innerText;
    let modified = Array.from(com.querySelectorAll('.uiLinkSubtle')).some(l => l.text === 'Modifié');
    return {
        'author': author,
        'content': content,
        'modified': modified
    };
}

return postToJson(arguments[0])
'''

import pickle
import json
from selenium import webdriver
import os
import time
from datetime import datetime
import random
from threading import Thread, Lock
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


cookie_file = os.path.join(os.path.dirname(__file__), 'facebook.pkl')
options = webdriver.ChromeOptions()
prefs = {"profile.managed_default_content_settings.images":2}
options.add_experimental_option("prefs",prefs)
options.add_argument('headless')

class Job:
    jobs = {}
    next_job_id = random.randint(0, 1000)

    def __init__(self, id, url, nb_posts):
        self.url = url
        self.nb_posts = nb_posts
        self.id = id
        self.progress = 0
        self.posts = []
        self.lock = Lock()
        self.driver = None



#    def fetch_posts(self):
#        with open('/home/ptk/Projets/auto_note/flask_note/app/static/fbposts.json') as f:
#            posts = json.load(f)
#
#        for (i, post) in enumerate(posts[:self.nb_posts]):
#            self.posts.append(post)
#            self.progress = i + 1
#            time.sleep(0.01)
 
    def fetch_posts(self):
        with self.lock:
            self.driver = webdriver.Chrome(chrome_options=options)
            self.driver.get('https://facebook.com')
            for cookie in pickle.load(open(cookie_file, 'rb')):
                self.driver.add_cookie(cookie)
            self.driver.get(self.url)
            self.driver.execute_async_script(do_until_js + scroll_js, self.nb_posts)
            post_els = self.driver.execute_script(
                "return Array.from(document.getElementsByClassName('userContentWrapper')).slice(0,arguments[0])",
                self.nb_posts
            )
            print(len(post_els))
            for i, el in enumerate(post_els):
                self.driver.execute_async_script(do_until_js + expand_comments_js, el)
                self.driver.execute_async_script(do_until_js + expand_post_js, el)
                post = self.driver.execute_script(parse_js, el)
                self.posts.append(post)
                self.progress = i + 1

    def _comment(self, permalink, content):
        with self.lock:
            self.driver.get(permalink)
            self.driver.find_element_by_class_name('UFIAddCommentInput').click()
            time.sleep(1)
            el = self.driver.find_element_by_css_selector("div[contenteditable=true]")
            el.send_keys(content)
            el.send_keys(Keys.RETURN)

    @classmethod
    def create_job(cls, url, nb_posts):
        id = cls.next_job_id
        cls.next_job_id += random.randint(0, 1000)
        cls.jobs[id] = cls(id, url, nb_posts)
        job_thread = Thread(target=cls.jobs[id].fetch_posts)
        job_thread.start()
        return id

    @classmethod
    def get_progress(cls, job_id):
        try:
            return cls.jobs[job_id].progress
        except KeyError:
            return None

    @classmethod
    def get_posts(cls, job_id, n, offset=0):
        try:
            return cls.jobs[job_id].posts[offset:offset+n+1]
        except KeyError:
            return None

    @classmethod
    def comment(cls, id, permalink, content):
        try:
            comment_thread = Thread(target=cls.jobs[id]._comment, args=(permalink, content))
            comment_thread.start()
            return True
        except KeyError:
            return None


if __name__ == '__main__':
    job_id = Job.create_job('https://www.facebook.com/groups/senquizz/', 10)
    n_retrieved_posts = 0
    while (n_retrieved_posts < 10):
        time.sleep(1)
        n_retrieved_posts = Job.get_progress(job_id)
        print(n_retrieved_posts)
    print(Job.get_posts(job_id, 10))
