Vue.component('post', {
  props: ['post', 'index', 'points', 'cycle'],
  data: function () {
    return {
      accepted: Array(this.post.comments.length).fill(false),
      marks: [],
      marks_text: 'OUT',
    }
  },
  methods: {
    updateScore: function (comIndex) {
        this.accepted[comIndex] = !(this.accepted[comIndex]);
        this.computeMarks()
    },

    comment: function () {
      this.$emit('send-comment', this.index, this.marks_text);  // TODO: do commenting ourselve pass Job ID as prop
    },

    copyToClipboard: function () {
     var el = document.createElement('textarea');
     el.value = this.marks_text;
     el.setAttribute('readonly', '');
     el.style = {position: 'absolute', left: '-9999px'};
     document.body.appendChild(el);
     el.select();
     document.execCommand('copy');
     document.body.removeChild(el);
     $('#copy-alert').show('fade');
     window.setTimeout(() => $('#copy-alert').hide('fade'), 1000)
    },

    prepareCopy: function (marks) {
      // lft to right unicode caracter to deal with arabic names
      let ltr = String.fromCharCode(parseInt('0x200E',16));
      let ltrs = ltr + ' ';
      let str = ltr;
      for (mark of marks) {
        str = str + mark.name + ltrs + mark.score + ltrs;
      }
      this.marks_text = str;
    },

    computeMarks: function () {
      if (this.accepted.some(bool => bool)) {
        indicesCorrect = this.accepted.reduce((acc, el, i) => el ? acc.concat(i) : acc, [])

        let indices = []
        if (!this.cycle) {
          indices = indicesCorrect
        } else {
          for (let i = 0; indices.length < this.points.length; i = (i + 1) % indicesCorrect.length) {
            indices.push(indicesCorrect[i])
          }
        }

        let pointsCounter = new Proxy({}, {
            get: (target, name) => name in target ? target[name] : 0
        })

        for (let i = 0; i < indices.length; i++) {
          let author = this.post.comments[indices[i]].author;
          pointsCounter[author] =   pointsCounter[author] + this.points[i];
        }

        let notation = [];
        Object.keys(pointsCounter).forEach(author => {
          notation.push({ "name": author, "score": pointsCounter[author]})
        })
        notation.sort((a, b) => b.score - a.score);
        this.marks = notation;
        this.prepareCopy(notation);

      } else {
          this.marks = [];
          this.marks_text = 'OUT';
      }
      this.$root.$emit('notes-computed', this.index, this.marks);
    }
  },
})


Vue.component('comment', {
  props: ['comment', 'index'],
  data: function() {
    return {
      accepted: false
    }
  },
  methods: {
    toggleAccept: function () {
      this.accepted = !(this.accepted);
      this.$emit('toggle-accepted', this.index);
    }
  },
})


Vue.component('setupmodal', {
  props: [],
  data: function() {
    return {
      fbUrl: 'https://www.facebook.com/groups/445915295756153/?sorting_setting=CHRONOLOGICAL',
      nbPosts: 15,
      notationStr: '70,50,30,10,10,10',
      cycle: true,
      notation: null,
    }
  },
  methods: {
    validateNotation: function () {
      function isNumeric(value) {
        return !isNaN(value - parseFloat(value));
      };
      if (this.notationStr) {
        let result = this.notationStr.split(',').filter(x => x).map(Number);
        if (result.every(isNumeric)) {
          this.notation = result
          return true;
        }
      }
      return false;
    },

    validateUrl: function() {
      if (/^(https?:\/\/)?((w{3}\.)?)facebook.com\/.*/i.test(this.fbUrl)) {
        return true;
      } else {
        return false;
      }
    },

    validateNbPosts() {
      if (this.$refs['nbPosts'].validity.rangeUnderflow) {
       return false;
      }
      if (this.$refs['nbPosts'].validity.rangeOverflow) {
        return false;
      }
      return true
    },

    submitForm: function () {
      if (this.validateUrl() & this.validateNbPosts() & this.validateNotation()) {
        this.$emit(
          'new-config',
          {
            'fbUrl': this.fbUrl,
            'nbPosts': Number(this.nbPosts),
            'notation': this.notation,
            'cycle': this.cycle,
          }
        );
        $('#inputmodal').modal('hide');
      } else {
        console.log('nope')  // TODO
      }
    },
  },
})



Vue.component('teams', {
  props: [],
  data: function() {
    return {
      players_set: new Set(),
      players: [],
      colorMap: {},
      playersTeam: {},
    }
  },

  computed: {
  },

  methods: {
    addPlayers: function(_, postScores) {
      postScores.forEach(notation => {this.players_set.add(notation.name)})
      this.players = Array.from(this.players_set)
    },
    addTeam: function (name, rgbColor) {
      if ((name !== '') & !(name in Object.keys(this.colorMap)) ){
        Vue.set(this.colorMap, name, rgbColor)
        return true;
      } else {
        return false
      }
    },
    addTeamFromForm: function() {
      name = this.$refs['team'].value
      rgbColor = this.$refs['color'].value
      if (this.addTeam(name, rgbColor)) {
        this.$refs['team'].value = '';
      }
    },
    deleteTeam: function(team) {
      Vue.delete(this.colorMap, team)
      let assignedPlayers = Object.keys(this.playersTeam);
      for(let player of assignedPlayers) {
        console.log(player)
        console.log(this.playersTeam[player])
        if (this.playersTeam[player] === team) {
          Vue.delete(this.playersTeam, player);
        }
      }
    },
    computeGlobalRanking: function() {
      let nbAssigned = Object.keys(this.playersTeam).length
      let nbTeams = Object.keys(this.colorMap).length
      if ((nbTeams !== 0) & (this.players.length !== nbAssigned)) {  // TODO: should be ok if user does not modify js directly
        console.log('not all players have been assigned to a team');  // TODO: should tell user
      } else {
        this.$root.$emit('teams-set', this.colorMap, this.playersTeam);
      }
    }
  },
  created: function () {
    this.$root.$on('notes-computed', this.addPlayers);
    this.$root.$on('add-team', this.addTeam);
  },

  beforeDestroy: function () {
    this.$root.$off('notes-computed', this.addPlayers);
    this.$root.$off('add-team', this.addTeam);
  }
})


Vue.component('results', {
  props: [],
  data: function() {
    return {
      postsScores: [],
      notes: [],
      totalPoints: 0,
      nbAnswers: 0,
      colorMap: null,
      playersTeam: null,
      evenColor: '#e3e3e3',
      oddColor: '#ffffff'
    }
  },

  computed: {
    existGroups: function() {
      return (this.playersTeam !== null)
    },

    notesPerTeam: function() {
      if(this.existGroups & (this.notes.length > 0)) {
        let pointsCounter = new Proxy({}, {
            get: (target, name) => name in target ? target[name] : 0
        })

        let nbRepsCounter = new Proxy({}, {
            get: (target, name) => name in target ? target[name] : 0
        })

        for (note of this.notes) {
          let team = this.playersTeam[note.name]
          pointsCounter[team] = pointsCounter[team] + note.score;
          nbRepsCounter[team] = nbRepsCounter[team] + note.count;
        }

        let teamsMarks = []
        for (let team of Object.keys(pointsCounter)) {
          teamsMarks.push(
            {
              "name": team,
              "score": pointsCounter[team],
              "count": nbRepsCounter[team],
              "rank": null
            }
          );
        }

        teamsMarks.sort(function(a, b){return b.score - a.score});

        teamsMarks[0].rank = 1
        for (let i=1; i < notation.length; i++) {
          if (teamsMarks[i].score == teamsMarks[i-1].score) {
            teamsMarks[i].rank = teamsMarks[i-1].rank;
          } else {
            teamsMarks[i].rank = i + 1;
          }
        }

        return teamsMarks;

      } else {
        return null;
      }
    }
  },

  methods: {
    getColor: function(name, index) {
      if (this.playersTeam === null) {
        if (index % 2 === 1) {
          return this.oddColor;
        } else {
          return this.evenColor;
        }
      } else {
        return this.colorMap[this.playersTeam[name]];
      }
    },

    setPostMarks: function(index, marks) {
      this.postsScores[index] = marks;
    },

    setColorsAndTeams: function(colorMap, playersTeam) {
        if (Object.keys(colorMap).length !== 0) {
          this.colorMap = colorMap;
          this.playersTeam = playersTeam;
        } else {
          this.colorMap = null;
          this.playersTeam = null;
        }
    },

    computeResults: function(colorMap, playersTeam) {
      this.setColorsAndTeams(colorMap, playersTeam)
      let pointsCounter = new Proxy({}, {
          get: (target, name) => name in target ? target[name] : {total: 0, count: 0},
          set: (target, name, value) => {
            if (name in target) {
              target[name].total = target[name].total + value;
              target[name].count = target[name].count + 1;
            } else {
              target[name] = {total: value, count: 1}
            }
          }
      })

      this.postsScores.forEach(postScores => {
        postScores.forEach(notation => {
          pointsCounter[notation.name] =  notation.score
        })
      })

      let notation = []
      for (author of Object.keys(pointsCounter)) {
        let note = {
          "name": author,
          "score": pointsCounter[author].total,
          "count": pointsCounter[author].count,
          "rank": null
        };
        notation.push(note);
      }

      notation.sort(function(a, b){return b.score - a.score});

      notation[0].rank = 1
      for (let i=1; i < notation.length; i++) {
        if (notation[i].score == notation[i-1].score) {
          notation[i].rank = notation[i-1].rank;
        } else {
          notation[i].rank = i + 1;
        }
      }

      this.totalPoints = notation.reduce((acc, val) => {return acc + val.score} , 0);
      this.nbAnswers = notation.reduce((acc, val) =>  {return acc + val.count} , 0);
      this.notes = notation;
    },

    downloadNotationAsCSV: function() {
      let csvstr = 'rang,nom,points,reponses'
      this.notes.forEach((el) => {
        csvstr = csvstr + '\n' + el.rank + ',' + el.name + ',' + el.score + ',' + el.count;
      })
      csvstr = csvstr + '\n' + 'total,' +',' + this.totalPoints + ',' + this.nbAnswers
      csvstr = 'data:text/csv;charset=utf-8,' + csvstr
      let encodedUri = encodeURI(csvstr);
      let link = document.createElement('a');
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "notation.csv");
      link.click();
    }
  },

  created: function () {
    this.$root.$on('notes-computed', this.setPostMarks);
    this.$root.$on('teams-set', this.computeResults);

  },

  beforeDestroy: function () {
    this.$root.$off('notes-computed', this.setPostMarks);
    this.$root.$off('teams-set', this.computeResults);

  }
})


new Vue({
  el: '#app',

  data: function() {
    return {
      config: {
        jobId: 0,
        nbProcessed: 0,
        posts: [],
        notation: null,
        nbPosts: null,
        fbUrl: null,
        cycle: null,
      }
    }
  },

  methods: {
    resetState: function() {
      this.config = {
        jobId: 0,
        nbProcessed: 0,
        posts: [],
        notation: null,
        nbPosts: null,
        fbUrl: null,
        cycle: null,
      }
    },

    setConfig: function(config) {
      this.resetState();
      this.config.fbUrl = config.fbUrl;
      this.config.cycle = config.cycle;
      this.config.nbPosts = config.nbPosts;
      this.config.notation = config.notation;
      this.config.postsScores = Array(config.nbPosts).fill([]);
      this.config.globalNotation = {notes:[], total: {points: 0, count: 0},};
      this.fetchPosts(this.config.fbUrl, this.config.nbPosts);
    },

    getNextPosts: function() {
      return new Promise(async (resolve, reject) => {
        let offset = this.config.posts.length
        let n = this.config.nbProcessed - offset
        let queryUrl = '/posts?' + 'id=' + this.config.jobId + '&nbposts=' + n + '&offset=' + offset
        let response = await fetch(queryUrl, {method: 'GET', dataType: 'json'})
        let posts = await response.json()
        this.config.posts.push(...posts)
        resolve()
      })
    },

    getProgress: function () {
      return new Promise(async (resolve, reject) => {
        let queryUrl = '/progress?' + 'id=' + this.config.jobId
        let response = await fetch(queryUrl, {method: 'GET', dataType: 'json'})
        let nbProcessed = await response.json()
        this.config.nbProcessed = nbProcessed
        resolve()
      })
    },

    fetchPosts: async function (url, nbPosts) {
      let queryUrl = '/submit' + '?' + 'fburl=' + encodeURIComponent(url) + '&nbposts=' + nbPosts
      let response = await fetch(queryUrl, {method: 'GET', dataType: 'json'})
      let jobId = await response.json()
      this.config.jobId = jobId
      self = this;
      window.setTimeout(
        async function retrieve() {
          await self.getProgress()
          if (self.config.posts.length < self.config.nbProcessed) {
            await self.getNextPosts()
            window.setTimeout(retrieve, 1000)
          } else if (self.config.nbProcessed !== self.config.nbPosts) {
            window.setTimeout(retrieve, 1000)
          }
        }, 1000
      )
    },

    sendComment: async function (post_index, content) {
      let permalink = this.config.posts[post_index]['permalink'];
      response = await fetch('/comment', {
        method: 'POST',
        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
        body: JSON.stringify({'id': this.config.jobId, 'permalink': permalink, 'text': content})
      });
    },
  },
})
