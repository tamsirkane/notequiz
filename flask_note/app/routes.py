from flask import render_template
from flask import jsonify
from flask import request
from auto_note.flask_note.app import app
from auto_note.fetch.fetch import Job
import json
import re


@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html')


def validate_url(url):
    if re.match(r"^(https?://)?(www\.)facebook.com/.*", url):
        return True
    else:
        return False


def validate_nb(n_str, low, high):
    try:
        n = int(n_str)
    except ValueError:
        return False
    else:
        if ((low <= n) and (n <= high)):
            return True
        else:
            return False


@app.route('/submit', methods=['GET'])
def submitjob():
    fb_url = request.args.get('fburl')
    nb_posts_str = request.args.get('nbposts')
    if validate_url(fb_url) and validate_nb(nb_posts_str, 1, 50):
        job_id = Job.create_job(fb_url, int(nb_posts_str))
        return jsonify(job_id)
    else:
        pass  # TODO


@app.route('/progress', methods=['GET'])
def progress():
    job_id = int(request.args.get('id'))
    nb_processed = Job.get_progress(job_id)
    return jsonify(nb_processed)


@app.route('/posts', methods=['GET'])
def posts():
    job_id = request.args.get('id')
    n = request.args.get('nbposts')
    offset = request.args.get('offset')
    if (validate_nb(n, 0, float('inf')) and
        validate_nb(offset, 0, float('inf')) and
        validate_nb(job_id, 0, float('inf'))
    ):
        return jsonify(Job.get_posts(int(job_id), int(n), int(offset)))
    else:
        pass  # TODO


@app.route('/comment', methods=['POST'])
def comment():
    data = request.get_json() or {}
    job_id = int(data.get('id'))
    permalink = data.get('permalink')
    content = data.get('text')
    ok = Job.comment(job_id, permalink, content)
    if(ok):
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    else:
        pass  # TODO
